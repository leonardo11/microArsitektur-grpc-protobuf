module mikroarsitek

go 1.14

require (
	github.com/golang/protobuf v1.4.2
	github.com/google/go-cmp v0.5.0 // indirect
	golang.org/x/net v0.0.0-20200707034311-ab3426394381 // indirect
	golang.org/x/sys v0.0.0-20200727154430-2d971f7391a4 // indirect
	golang.org/x/text v0.3.3 // indirect
	google.golang.org/grpc v1.30.0
	google.golang.org/protobuf v1.23.0
)
